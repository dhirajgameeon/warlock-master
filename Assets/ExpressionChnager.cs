using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpressionChnager : MonoBehaviour
{
    private controlPriest ControlPriest;
    public SkinnedMeshRenderer Eye, Lip;
    public Texture[] Eyes, Lips;
    public float TimeStamp;



    // Start is called before the first frame update
    void Awake()
    {
        ControlPriest = GetComponentInParent<controlPriest>();
    }
    private void Start()
    {
        OpenEye();
    }
    // Update is called once per frame
    void Update()
    {
        ExprassionChange();
        /*contantFaceExpression();*/
    }

    void ExprassionChange()
    {
        if (ControlPriest.isStunned)
        {
            CloseEye();
        }
        if (!ControlPriest.isStunned)
        {
            OpenEye();
        }
    }

/*    float x = 1;
    void contantFaceExpression()
    {
        if (x > 0) x -= Time.deltaTime;

        if (x >= TimeStamp / 2)
        {
            OpenEye();
        }
        if(x<= TimeStamp / 2)
        {
            CloseEye();
        }
        if (x <= 0)
        {            
            x = TimeStamp;
        }
    }*/

    void OpenEye()
    {
        Eye.material.mainTexture = Eyes[1];
        Lip.material.mainTexture = Lips[1];
    }
    void CloseEye()
    {
        Eye.material.mainTexture = Eyes[0];
        Lip.material.mainTexture = Lips[0];
    }
}
