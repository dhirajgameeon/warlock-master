using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrailMover : MonoBehaviour
{
    public bool isMove = false;
    public Transform Destination;
    public Vector3 offset;
    public float trailSpeed = 0.5f;
    // Start is called before the first frame update
    void Start()
    {
        LeanTween.move(gameObject, Destination.position + offset, trailSpeed).setOnComplete(() => { Destroy(this.gameObject, 0.15f); });
    }

    // Update is called once per frame
    void Update()
    {
        //Move();
    }
    void Move()
    {
        if (Destination && isMove)
        {
            transform.position = Vector3.MoveTowards(transform.position, Destination.position + offset, trailSpeed * Time.deltaTime);
            if (Vector3.Distance(transform.position, Destination.position + offset) <= 0.05f)
            {
                
            }
        }

    }
}
