using UnityEngine;

public enum AnimationState
{
    Idle,
    Move,
    Death,
    Victory,
    Attack
}
public class AnimationHandler : MonoBehaviour
{
    public Animator anim;

    public void playAnim(AnimationState state, float speed = 1, bool loop = true)
    {
        switch (state)
        {
            case AnimationState.Idle:
                IdleAnimation();
                break;
            case AnimationState.Move:
                RunAnimation(speed);
                break;
            case AnimationState.Death:
                DeathAnimation();
                break;
            case AnimationState.Victory:
                victory();
                break;
            case AnimationState.Attack:
                Attack(loop);
                break;
        }
    }
    public void AimationPlay(string animationName)
    {
        anim.Play(animationName);
    }
    void IdleAnimation()
    {
        anim.Play("Idle");
    }
    void RunAnimation(float speed)
    {
        try { anim.SetFloat("speed", speed); } catch { }
    }
    void DeathAnimation()
    {
        anim.Play("Death");
    }
    void victory()
    {
        anim.SetTrigger("victory");
    }
    void Attack(bool isA)
    {
        try { anim.SetBool("isAttacking", isA); } catch { }
    }
}
