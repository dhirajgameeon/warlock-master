using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateHitboxes : MonoBehaviour
{

    public List<TakeDamageType>TDT=new List<TakeDamageType>();
    public movePriest mv;
    private void Awake()
    {
        mv = GetComponent<movePriest>();
    }
    // Start is called before the first frame update
    void Start()
    {
/*        foreach (TakeDamageType item in TDT)
        {
            if (item.DoNotInclude) item.DoNotInclude = false;
        }*/
    }

    // Update is called once per frame
    void Update()
    {
        resetAll();
    }
    void resetAll()
    {
        if (mv.isArmyDead)
        {
            foreach (TakeDamageType item in TDT)
            {
                if (item.DoNotInclude) item.DoNotInclude = false;
            }
        }
    }
}
