using UnityEngine;

[System.Serializable]
public class CustomAttributes 
{
    [Header("Gethering Data")]
    public Color checkingColor;
    public int checkingRedius;

    [Header("Locomotion Data")]
    public float MovementSpeed;
    public float TurnRate;
    public float stunTime;

    [Header("Health Data")]
    public float HP;
    public float AttackSpeed;
    public float HeadDamage;
    public float BodyDamage;
    public float LegDamage;
    public CustomAttributes()
    {
        checkingColor = Color.white;
        checkingRedius = 0;
        MovementSpeed = 0;
        TurnRate = 0;
        stunTime = 0;

        HP = 100;
        AttackSpeed = 1;
        HeadDamage = 10;
        BodyDamage = 2;
        LegDamage = 1;
    }
}
