public interface IDamageable {
    void TakeDamage(collisionType damageType);
    void TakeDamage(collisionType damageType, float damage, int i = 0);
}

