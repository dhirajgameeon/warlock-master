using UnityEngine;

public class AnimationPriest : MonoBehaviour
{
    private AnimationHandler anim;
    private movePriest move;
    private void Awake()
    {
        anim = GetComponent<AnimationHandler>();
        move = GetComponent<movePriest>();
        
    }

    void Start()
    {
        
    }

    void Update()
    {
        isMoving();
        try
        {
            isAttack();
        }
        catch (System.Exception)
        {

            throw;
        }
        
    }
    void isMoving()
    {
        anim.playAnim(AnimationState.Move, move.agent.velocity.magnitude);
        /*if (move.isMoving) ; else { anim.playAnim(AnimationState.Move, 0); }*/
    }
    void isAttack()
    {
        anim.playAnim(AnimationState.Attack, 1, move.isAttacking);
    }
}
