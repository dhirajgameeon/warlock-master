using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class PLookAt : MonoBehaviour
{
    public CustomAttributes PriestStat;
    public Transform Player;
    public controlPriest control;
    public ParticleSystem HealVFX;

    movePriest movePriest;
    attackPriest atkP;
    private void Awake()
    {
        atkP = GetComponent<attackPriest>();
        movePriest = GetComponent<movePriest>();
        Player = GameObject.Find("Player").transform;
        control = GetComponent<controlPriest>();   
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!movePriest.isMoving && movePriest.isAttacking ) smoothLookat(atkP.target.position, 10);
    }
    public void smoothLookat(Vector3 target, float rotationSpeed)
    {
        var targetPos = target;
        targetPos.y = transform.position.y;
        var targetDir = Quaternion.LookRotation(targetPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, rotationSpeed * Time.deltaTime);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = PriestStat.checkingColor;
        Handles.DrawWireDisc(transform.position, Vector3.up, PriestStat.checkingRedius, 2);
    }
#endif
}
