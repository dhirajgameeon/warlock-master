using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeaderPristHeal : MonoBehaviour
{
    public GameObject lineRenderer;
    public Transform shootPoint;
    public ParticleSystem HealVFX;
    public float healAmount = 10;
    public float healRate = 0.5f;

    private PLookAt pat;
    private movePriest move;
    private float nextTimeHeal = 0;
    private AnimationHandler anim;
    private void Awake()
    {
        anim = GetComponent<AnimationHandler>();
        pat = GetComponent<PLookAt>();
    }

    private void Start()
    {
        move = GetComponent<movePriest>();
    }

    private void Update()
    {
        if(move.isActive && !move.controlPriest.isDead) Heal();
       // if(!move.isArmyDead &  !move.isAttacking) /*lineRenderer.SetPosition(1, new Vector3(0, 0, 0));*/
    }
    void Heal()
    {
        if (nextTimeHeal > 0 && !move.isArmyDead) nextTimeHeal -= Time.deltaTime;
        if(nextTimeHeal <=0)
        {
            foreach (controlPriest item in move.armyPrist)
            {
                if (item.priest.PriestStat.HP < 100)
                {                    
                    move.isAttacking = true;
                    //pat.smoothLookat(item.transform.position, 5);
                }                        
            }
                    LeanTween.delayedCall(0.35f,() => {
                foreach (controlPriest item in move.armyPrist)
                {
                    if (item.priest.PriestStat.HP < 100 && !item.priest.control.isDead)
                    {
                                GameObject trail = Instantiate(lineRenderer, shootPoint.position, Quaternion.identity);
                                trail.GetComponent<TrailMover>().Destination = item.GetComponent<controlPriest>().headPosition;
                                trail.GetComponent<TrailMover>().isMove = true;



/*                                LeanTween.move(trail, item.GetComponent<controlPriest>().headPosition.position + new Vector3(0, -0.2f, -0.3f), 0.4f).setOnComplete(() => {
                                    LeanTween.delayedCall(0.25f, () => { Destroy(trail); });
                                });*/
                                item.priest.PriestStat.HP += healAmount;
                                item.priest.HealVFX.Play();
                                HealVFX.Play();
                    }                    
                }
            });
            nextTimeHeal = healRate;
        }
    }

/*    void attack()
    {
        anim.playAnim(AnimationState.Attack, 1, move.isAttacking);
    }*/
}
