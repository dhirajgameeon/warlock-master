using UnityEngine;
using UnityEngine.AI;

public class attackPriest : MonoBehaviour
{
    public bool isLeader = false;
    public GameObject[] Projectile;
    public Transform SpwanPoint;
    public Transform target;

    public LayerMask detectLayer;
    public float detactionRange = 3f;
    public float shootingOffset = 1;
    public int Damage = 25;
    public int currentProjectile = 0;
    public float AttakeRate = 1f;
    private float nextTimeAttack = 0;
    public float speed = 3500;

    public Animator anim;
    public NavMeshAgent Agent;
    public movePriest Priest;
    private Transform Player;
    private PLookAt priestLookAt;
    private void Awake()
    {
        priestLookAt = GetComponent<PLookAt>();
        Priest = GetComponent<movePriest>();
        Player = GameObject.Find("Player").transform;
        nextTimeAttack = AttakeRate;
    }
    // Start is called before the first frame update
    void Start()
    {
        nextTimeAttack = AttakeRate;
        target = Player;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameManager.instance.isGameComplete && !GameManager.instance.isPlayerDied) 
            WhenToAttack();

        checkForNearByDemon();


        //if(Priest.controlPriest.isStunned) Priest.isAttacking = !Priest.controlPriest.isStunned;
    }

    public void WhenToAttack()
    {
        if (!Priest.controlPriest.isStunned && Priest.isActive)
        {
            if (isLeader && Priest.isArmyDead)
            {
                /*print(transform.name + " LAtk");*/
                shoot(0.35f);
            }


            if (!isLeader)
            {
                if (Priest.isStopped || Priest.agent.velocity.magnitude < 0.1f)
                {
                    /*print(transform.name + " HAtk");*/
                    shoot(0.75f);
                }
            }
        }
    }
    [SerializeField]Collider[] demon;
    void checkForNearByDemon()
    {
        demon = Physics.OverlapSphere(transform.position, detactionRange, detectLayer);

        if (demon.Length > 0)
        {
            target = demon[demon.Length - 1].transform;
        }
    }

    Vector3 AttackPosition(Vector3 player)
    {
        Vector3 shootPoint = new Vector3(Random.Range(player.x- shootingOffset, player.x+ shootingOffset), Random.Range(player.y - shootingOffset, player.y + shootingOffset),player.z);
        return shootPoint;
    }
    void shoot(float delay)
    {
        if (nextTimeAttack > 0)
        {
            nextTimeAttack -= Time.deltaTime;
            if (nextTimeAttack <= AttakeRate / 3) { 
                Priest.isAttacking = true;
            }
        }
        if (nextTimeAttack <= 0)
        {
            projectileFireBall(AttackPosition(target.position + new Vector3(0, 1, 0)), Damage, delay);
            nextTimeAttack = AttakeRate;
        }
    }
    /*void shoot(float t)
    {
        if (nextTimeAttack > 0)
        {
            nextTimeAttack -= Time.deltaTime;
            if (nextTimeAttack <= AttakeRate/1.5f) Priest.isAttacking = true;
        }
        if (nextTimeAttack <= 0)
        {
            projectileFireBall(AttackPosition(target.position + new Vector3(0, 1, 0)), Damage, 0.25f);
            nextTimeAttack = AttakeRate;
        }
    }*/

    void projectileFireBall(Vector3 position, float Damage, float delay)
    {
        LeanTween.delayedCall(delay, () => { Priest.isAttacking = false; });
        GameObject projectile = Instantiate(Projectile[currentProjectile], SpwanPoint.position, Quaternion.identity);
        projectile.transform.LookAt(position);
        projectile.GetComponent<impact>().damage = Damage;
        projectile.GetComponent<Rigidbody>().AddForce(projectile.transform.forward * speed);
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PriestProjectile);
    }
}
