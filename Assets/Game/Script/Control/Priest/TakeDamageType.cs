using UnityEngine;
using UnityEngine.Animations.Rigging;
public enum collisionType { body = 0,
    head = 1,
    legs = 2 
}
public class TakeDamageType : MonoBehaviour, IDamageable
{
    public collisionType DamageType;
    private PLookAt priest;
    public bool isTargeted = false;
    public bool isDead = false;
    public bool DoNotInclude = false;
    public GameObject boneRender;
    public attackPriest atkP;
    private void Awake()
    {
        atkP=GetComponentInParent<attackPriest>();
        priest = GetComponentInParent<PLookAt>();
        boneRender = transform.GetComponentInParent<BoneRenderer>().gameObject;
    }

    private void OnDisable()
    {
        isDead = priest.control.isDead;
    }
    private void Update()
    {
        if (!this.gameObject.activeSelf) isDead = priest.control.isDead;

        isDead = priest.control.isDead;
    }
    public void TakeDamage(collisionType damageType)
    {
        switch (damageType)
        {
            case collisionType.body:
                priest.PriestStat.HP -= priest.PriestStat.BodyDamage;
                break;
            case collisionType.head:
                priest.PriestStat.HP -= priest.PriestStat.HeadDamage;
                break;
            case collisionType.legs:
                priest.PriestStat.HP -= priest.PriestStat.LegDamage;
                break;
        }
        if (!priest.control.isStunned) priest.control.isStunned = true;
        atkP.anim.SetInteger("ranHit", Random.Range(0, 4));
        atkP.Priest.isStopped = true;
    }
    public void TakeDamage(collisionType damageType, float damage,int i =0)
    {
        switch (damageType)
        {
            case collisionType.body:
                priest.PriestStat.HP -= damage;                
                break;

            case collisionType.head:
                priest.PriestStat.HP -= damage;                
                break;

            case collisionType.legs:
                priest.PriestStat.HP -= damage;                
                break;

        }
        if (!priest.control.isStunned) priest.control.isStunned = true;
        if (i == 0) atkP.anim.SetInteger("ranHit", Random.Range(0, 4));
        if (i == 1) { 
            atkP.anim.Play("Hit 3");
            //atkP.anim.speed = 1;
        }
        atkP.Priest.isStopped = true;
    }

}
