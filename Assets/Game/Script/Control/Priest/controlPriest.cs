using UnityEngine;
using UnityEngine.UI;

public class controlPriest : MonoBehaviour, IDamageable
{
    public int GiveCoinCount;

    public bool isStunned = false;
    public bool isDead = false;

    public Animator anim;

    public ParticleSystem stun;
    public GameObject SoulParticle;
    public GameObject MeshRender;
    public GameObject GFXRender;
    public ParticleSystem partical;
    public Transform headPosition;

    [Header("Health")]
    public Slider HealthBar;
    public float USpeed;
    public float DSpeed;

    [HideInInspector]public PLookAt priest;
    [HideInInspector] public movePriest move;
    private float stunTime = 1;
    private Transform player;
    private Transform SoulCollector;
    void Awake()
    {
        priest = GetComponent<PLookAt>();
        move = GetComponent<movePriest>();
        player = GameObject.Find("Player").transform;
        SoulCollector = GameObject.FindGameObjectWithTag("sC").transform;
    }

    private void Start()
    {
        stunTime = priest.PriestStat.stunTime;
        HealthBar.maxValue = priest.PriestStat.HP;
        currentProgress = priest.PriestStat.HP;
    }

    void Update()
    {
        isStun();
        dead();
        HealthBar.value = progressSlider(priest.PriestStat.HP);
        HealthBar.transform.forward = Camera.main.transform.forward;
    }

    void dead()
    {
        if (priest.PriestStat.HP <= 0 && !isDead)
        {
            toDoAfterDeath();
            spawnSoul();
            isDead = true;
        }
    }

    void spawnSoul()
    {
        GameObject soul = Instantiate(SoulParticle, transform.position, Quaternion.identity);
        LeanTween.delayedCall(.25f, () => {
            LeanTween.move(soul, player.transform.position + new Vector3(0, 1, 0)/*SoulCollector.position*/, 0.5f).setOnComplete(() => {
                if (GameManager.instance.SoulCount < 5) 
                {
                    GameManager.instance.SoulCount += 1;
                    GameManager.instance.SoulAnimation.Play("Glow");
                    //PlayerPrefs.SetInt("TotalSoul", GameManager.instance.SC);
                    if (TutorialManager.instance.SoulCount < GameManager.instance.SoulCount) PlayerPrefs.SetInt("soul", GameManager.instance.SoulCount); 
                }
                Destroy(soul, 0.1f);
            });
        });
        
    }
    

    void toDoAfterDeath()
    {
        transform.tag = "Untagged";        
        if (MeshRender.activeSelf)
        {
            LeanTween.delayedCall(0.15f, () => { GFXRender.SetActive(false); });
            MeshRender.SetActive(false);
        }
        anim.Play("Dead");
        partical.Play();
        //GetComponent<Collider>().enabled = false;
        try { GetComponent<attackPriest>().enabled = false; } catch { }                
        GameManager.instance.maxCash += GiveCoinCount;
        PlayerPrefs.SetFloat("Coin", GameManager.instance.maxCash);
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PriestDeath);

    }

    float currentProgress;
    float progressSlider(float maxProgress)
    {
        if (currentProgress < maxProgress)
        {
            currentProgress += USpeed * Time.deltaTime;
            if (currentProgress >= maxProgress) currentProgress = maxProgress;
        }
        if (currentProgress > maxProgress)
        {
            currentProgress -= DSpeed * Time.deltaTime;
            if (currentProgress <= maxProgress) currentProgress = maxProgress;
        }
        if (currentProgress == maxProgress)
            currentProgress = maxProgress;

        return currentProgress;
    }
    void isStun()
    {
        if (isStunned && stunTime > 0 && !isDead)
        {
            stunTime -= Time.deltaTime;
            if (stunTime <= 0)
            {
                stunTime = priest.PriestStat.stunTime;
                isStunned = false;
            }
        }
    }

    public void TakeDamage(collisionType damageType)
    {
        priest.PriestStat.HP -= priest.PriestStat.LegDamage;
        if(!isStunned)isStunned = true;
    }
    public void TakeDamage(collisionType damageType, float damage, int i = 0)
    {
        //priest.PriestStat.HP -= priest.PriestStat.LegDamage;
        //isStunned = true;
    }
}
