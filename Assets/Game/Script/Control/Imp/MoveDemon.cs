using UnityEngine;
using UnityEngine.AI;

public class MoveDemon : MonoBehaviour
{
    public int NeedSoul;
    public float stoppingDistance = 1;
    public float moveSpeed = 3;
    public bool isStopped = false;
    [SerializeField] private float Distance;


    //public Vector3 Pos1, Pos2;
    public Vector3 offset;
    public Vector3 agentPostionToWalk;
    public float rotationSmooth = 0.2f;
    private Transform FPO;
    private float turnSmoothVelocity;
    private bool spawnComplete = false;
    public NavMeshAgent agent;
    public Transform Player;
    private ControlDemon control;
    private movePlayer MP;
    private void Awake()
    {
        FPO = GameObject.FindGameObjectWithTag("FPO").transform;
        Player = GameObject.Find("Player").transform;
        control = GetComponent<ControlDemon>();
        agent = GetComponent<NavMeshAgent>();
        MP = FindObjectOfType<movePlayer>();
        spawnComplete = true;
        agent.enabled = !spawnComplete;
    }

    void Start()
    {
        //agent.stoppingDistance = stoppingDistance;
        
        if (spawnComplete) SpawnFromPortel();
        if (GameManager.instance.SoulCount > 0)
        {
          //  PlayerPrefs.SetInt("TotalSoul", GameManager.instance.SC);
            GameManager.instance.SoulCount -= NeedSoul;
        }
        
    }

    private void OnEnable()
    {
       
    }

    bool isPosLocked = false;
    private void Update()
    {
        
        if (!spawnComplete)
        {
            AgentMovement();
            callAfterAgentActive();
            if (control.BodyPartObject) Distance = Vector3.Distance(transform.position, control.BodyPartObject.transform.position);
        }
        if (/*GameManager.instance.isGameComplete && !isPosLocked*/ MP.isMove)
        {
            agentPostionToWalk = postionTowalk(FPO.position, 2);
            /*isPosLocked = true;*/
        }
       
    }


    public void callAfterAgentActive()
    {
        if (agent.enabled)
        {
            if (!control.BodyPartObject)
            {
                //print("DamageIsNull");
                //agent.SetDestination(agentPostionToWalk);
            }
            if (agent.velocity.magnitude < 0.1f) isStopped = true; else if (agent.velocity.magnitude > 0) isStopped = false;
        }
        else
        {
            isStopped = false;
        }
    }
    private void LateUpdate()
    {
        //movePosition();
    }
    void movePosition()
    {
       // transform.position = Vector3.MoveTowards(transform.position, Player.position + offset, moveSpeed * Time.deltaTime);
    }
    void AgentMovement()
    {
        if (control.BodyPartObject) {
            agent.SetDestination(control.BodyPartObject.transform.position);


            //if (Vector3.Distance(transform.position, control.BodyPartObject.transform.position) <= stoppingDistance) isStopped = true; else { isStopped = false; }
            //agent.isStopped = isStopped;
        }
        
        
        if (agent.velocity.magnitude > 0.1f && !isStopped)
        {
            float targetAngle = Mathf.Atan2(agent.velocity.x, agent.velocity.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);
        }
    }

    void SpawnFromPortel()
    {
        LeanTween.move(gameObject,  new Vector3(transform.position.x, Player.position.y - 0.5f, transform.position.z), 0.25f).setOnComplete(() => {
            if (spawnComplete)
            {
                agent.enabled = true;
                spawnComplete = false;
            }
        });
    }


    Vector3 postionTowalk(Vector3 posi, float offset)
    {
        Vector3 pos = new Vector3(Random.Range(posi.x + offset, posi.x - offset), posi.y, posi.z);
        return pos;
    }
}
