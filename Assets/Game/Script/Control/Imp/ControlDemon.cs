using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlDemon : MonoBehaviour, IDamageable
{
    public List<TakeDamageType> bodyPart = new List<TakeDamageType>();
    public Animator anim;
    public ParticleSystem vfx_Hit;
    [Tooltip("Leg = 0, Head = 1, Body = 2")]
    public int bodyType = 0;

    public TakeDamageType BodyPartObject;

    public float damageDelay = 0.5f;
    public float Damage = 5;
    public float DamageRate = 5;
    private MoveDemon move;

    float x = 0.5f;
    private void Awake()
    {
        move = GetComponent<MoveDemon>();
        hitDamage = 0.75f;
        sfk();
        x = 0.5f;
    }

    void sfk()
    {
        if(bodyType==0) EvenManager.TriggerSFXOneShotPlayEvent(AudioID.Imp);
        if(bodyType==2) EvenManager.TriggerSFXOneShotPlayEvent(AudioID.Demon);
       
    }
    void Update()
    {
        if(!GameManager.instance.isGameComplete && !GameManager.instance.isPlayerDied)
        {
            bodyTypeChecker();
            checkNearLeg();
            takeHit();
            AnimationAct();
        }
        

        if(GameManager.instance.isGameComplete && move.agent.velocity.magnitude < 0.1f)
        {
            anim.Play("Dance");
            smoothLookat(move.Player.position, 5);
        }
    }
    public void bodyTypeChecker()
    {
        if(bodyType == 0) bodyPart = PriestManager.instance.Legs;
        if(bodyType == 1) bodyPart = PriestManager.instance.Head;
        if(bodyType == 2) bodyPart = PriestManager.instance.Body;
    }

    void checkNearLeg()
    {
        float closestDistance = Mathf.Infinity;
        if(BodyPartObject && BodyPartObject.isDead) BodyPartObject = null;

        foreach (TakeDamageType item in bodyPart)
        {
            if (!BodyPartObject && !item.isDead && !item.isTargeted)
            {
                float currentDistance;
                currentDistance = Vector3.Distance(transform.position, item.transform.position);
                if (currentDistance < closestDistance)
                {
                    closestDistance = currentDistance;
                    BodyPartObject = item;
                    BodyPartObject.isTargeted = true;
                    hitDamage = 0.75f;
                }
            }
            else
            {
                TargetNull();
            }
        }        
    }
    Collider[] coll;
    int i = 0;
    void TargetNull()
    {        
        if (!BodyPartObject && x > 0) x -= Time.deltaTime;


        if (!BodyPartObject && x<=0)
        {
             coll = Physics.OverlapSphere(transform.position, 15, LayerMask.GetMask("PriestHit"));
            if (bodyType == 0)
            {
                if(coll.Length>0 && i<= coll.Length - 1)
                {
                    if (coll[i].GetComponent<TakeDamageType>() && coll[i].GetComponent<TakeDamageType>().DamageType == collisionType.legs)
                        BodyPartObject = coll[i].GetComponent<TakeDamageType>();
                    else
                    {
                        i++;
                        return;
                    }

                    hitDamage = 0.75f;
                    x = 0.5f;
                }
                else if (i > coll.Length - 1)
                {
                    i = 0;
                }

                /* foreach (Collider item in coll)
                 {
                     if (item.GetComponent<TakeDamageType>() && item.GetComponent<TakeDamageType>().DamageType == collisionType.legs) 
                         BodyPartObject = item.GetComponent<TakeDamageType>();
                     hitDamage = 0.75f;
                     x = 0.5f;
                 }*/
            }
            if (bodyType == 2)
            {
                if (coll.Length > 0 && i <= coll.Length - 1)
                {
                    if (coll[i].GetComponent<TakeDamageType>() && coll[i].GetComponent<TakeDamageType>().DamageType == collisionType.body)
                        BodyPartObject = coll[i].GetComponent<TakeDamageType>();
                    else
                    {
                        i++;
                        return;
                    }
                    hitDamage = 0.75f;
                    x = 0.5f;
                }
                else if (i > coll.Length - 1)
                {
                    i = 0;
                }

                /*  foreach (Collider item in coll)
                  {
                      if (item.GetComponent<TakeDamageType>() && item.GetComponent<TakeDamageType>().DamageType == collisionType.body)
                          BodyPartObject = item.GetComponent<TakeDamageType>();
                      hitDamage = 0.75f;
                      x = 0.5f;
                  }*/
            }
        }
    }

    
    void smoothLookat(Vector3 target, float rotationSpeed)
    {
        //Debug.Log(transform.name + " Rotating...");
        var targetPos = target;
        targetPos.y = transform.position.y;
        var targetDir = Quaternion.LookRotation(targetPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, rotationSpeed * Time.deltaTime);
    }

    [SerializeField]float hitDamage;
    void takeHit()
    {
        if(BodyPartObject && move.isStopped)
        {
            smoothLookat(BodyPartObject.boneRender.transform.position, 10);
            BodyPartObject.atkP.target = this.gameObject.transform;
            BodyPartObject.atkP.Agent.stoppingDistance = 2.5f;
            BodyPartObject.atkP.Priest.stoppingDistance = 2.5f;
            if (hitDamage>0)hitDamage-=Time.deltaTime;
            if (hitDamage <= 0)
            {
                anim.SetTrigger("Attack");
                _ = LeanTween.delayedCall(damageDelay, () => {
                    BodyPartObject.TakeDamage(collisionType.legs, Damage);                    
                });
                hitDamage = DamageRate;
            }
            
        }
    }
    void AnimationAct()
    {
        anim.SetFloat("speed", move.agent.velocity.magnitude);
        
    }

    public void TakeDamage(collisionType damageType)
    {
        throw new System.NotImplementedException();
    }

    public void TakeDamage(collisionType damageType, float damage, int i = 0)
    {
        throw new System.NotImplementedException();
    }
}
