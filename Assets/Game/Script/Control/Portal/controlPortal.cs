using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class controlPortal : MonoBehaviour
{
    public float summonDelay;
    public bool isSizeComplete;
    public Vector3 Size;
    void Start()
    {
        summonTime = 0.2f;
        LeanTween.scale(gameObject, Size, 0.05f).setOnComplete(() => { isSizeComplete = true; });
    }

    // Update is called once per frame
    void Update()
    {
        if (isSizeComplete)
        {
            SummonCreator();
            if (GameManager.instance.SoulCount <= 0) Destroy(gameObject, 1.35f);
        }
      
    }
    void SummonCreator()
    {
        if (GameManager.instance.SoulCount >= 1 && GameManager.instance.SoulCount < 3)
        {
            summon(0);
        }
        if (GameManager.instance.SoulCount >= 3 && GameManager.instance.SoulCount < 5)
        {
            summon(1);
        }
        if (GameManager.instance.SoulCount >= 5)
        {
            summon(2);
        }
    }

    float summonTime;
    void summon(int summonType)
    {
        if (summonTime > 0) summonTime -= Time.deltaTime;
        if (summonTime <= 0)
        {
            Instantiate(GameManager.instance.summons[summonType],  new Vector3(transform.position.x, -2, transform.position.z), Quaternion.identity);
            summonTime = summonDelay;
        }
    }
}
