using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ProjectileAttack : MonoBehaviour
{
    

    public GameObject[] Projectile;
    public GameObject[] PortelVFX;
    public Transform SpwanPoint;
    public Transform cam;
    public int currentProjectile = 0;
    public float speed = 500;

    private AnimationHandler anim;
    private void Awake()
    {
        anim = GetComponent<AnimationHandler>();
    }
    void Start()
    {
        
    }


    void Update()
    {
        
    }
    public void ProjectileFireBall(Vector3 position, float damage)
    {
        anim.AimationPlay("Aim");
        LeanTween.delayedCall(0.25f, () =>
        {
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PlayerProjectile);
            GameObject projectile = Instantiate(Projectile[currentProjectile], SpwanPoint.position, Quaternion.identity);
            projectile.transform.LookAt(position);
            projectile.GetComponent<impact>().damage = damage;
            projectile.GetComponent<Rigidbody>().AddForce(projectile.transform.forward * speed);
        });
        
    }


    public void portelAnimation()
    {
        //anim.AimationPlay("Portel");
        anim.anim.SetBool("sum", true);
    }
    public void Portel(Vector3 position)
    {        
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PortelOpen);
        GameObject portel = Instantiate(PortelVFX[currentProjectile], position, Quaternion.Euler(0, 0, 0));
        anim.anim.SetBool("sum", false);
    }
}
