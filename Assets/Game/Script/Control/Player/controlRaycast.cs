using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;


public class controlRaycast : MonoBehaviour
{
    private PlayerInput _inputFromPlayer;
    public Camera Cam;
    public ParticleSystem vfx_Portel;
    public float fireRate = 1;
    public float portelSpeed = 1;
    public Vector3 PortelSpwanOffset;
    public LayerMask SelectionLayer;
    public LayerMask GroundMask;

    [SerializeField]bool isPortelOpen = false;
    LookAt PlayerLookAt;
    ProjectileAttack projectile;
    movePlayer playerMove;
    void Awake()
    {
        PlayerLookAt = GetComponent<LookAt>();
        projectile = GetComponent<ProjectileAttack>();
        playerMove = GetComponent<movePlayer>();
        _inputFromPlayer = new PlayerInput();
    }


    private void OnEnable()
    {
        _inputFromPlayer.PlayerPortel.Enable();
    }
    private void OnDisable()
    {
        _inputFromPlayer.PlayerPortel.Disable();
    }
    private void Start()
    {
        timer = 1;
        _inputFromPlayer.PlayerPortel.OpenPortel.performed += OpenPortel;
        _inputFromPlayer.PlayerPortel.OpenPortel.canceled += ShootProjectile;
    }


    void Update()
    {
        //mouseCheck();
        portelOpenUI();
        if (isPortelOpen && GameManager.instance.SoulCount <= 0) isPortelOpen = false;
    }


    public void ShootProjectile(InputAction.CallbackContext context)
    {
        
    }
    public void OpenPortel(InputAction.CallbackContext context)
    {
        if (!playerMove.isMoving && !GameManager.instance.isPlayerDied && !GameManager.instance.isGameComplete)
        {
            if (context.duration <= 0.6f) ShootFireBall(); else { if(!isPortelOpen && GameManager.instance.SoulCount>0) OpenPortel(); }
        }
    }

    float timer = 1;
    public bool portelTimerStart = false;
    public void portelOpenUI()
    {
        if (portelTimerStart)
        {
            GameManager.instance.SummonUI.gameObject.SetActive(true);

            if (timer > 0) timer -= portelSpeed * Time.deltaTime;
            float slider = timer / 1;

            GameManager.instance.circler.fillAmount = slider;
/*            GameManager.instance.TapTimer.text = timer.ToString("N2");*/
            if (timer <= 0)
            {
                //projectile.portelAnimation();
                //OpenPortel();
                portelTimerStart = false;
            }
        }
        if (!portelTimerStart)
        {
            timer = 1;
            GameManager.instance.circler.fillAmount = 1;
            GameManager.instance.SummonUI.gameObject.SetActive(false);
        }

    }

    void OpenPortel()
    {
        print("<color=green>Portel Openned</color>");
        OpenPortVFX();
        if (TutorialManager.instance.SoulCount == 1) { TutorialManager.instance.isHolding = true; TutorialManager.instance.TapAndHold.gameObject.SetActive(false); }
        if(TutorialManager.instance.SoulCount == 3) { TutorialManager.instance.isHolding1 = true; TutorialManager.instance.TapAndHold.gameObject.SetActive(false); }
        if (TutorialManager.instance.SoulCount == 5) { TutorialManager.instance.isHolding2 = true; TutorialManager.instance.TapAndHold.gameObject.SetActive(false); }
    }

    Vector3 mousePos;
    
    float nextTimeAttack = 0;
    void ShootFireBall()
    {
        if(Time.time >= nextTimeAttack)
        {
            if (Physics.Raycast(Cam.ScreenPointToRay(Input.mousePosition), out RaycastHit hit, 10000,SelectionLayer))
            {
                projectile.ProjectileFireBall(hit.point, PlayerLookAt.PlayerStat.BodyDamage);
                nextTimeAttack = Time.time + 1f / fireRate;
            }            
        }
    }
    RaycastHit hit;
    void OpenPortVFX()
    {
        if (Physics.Raycast(Cam.ScreenPointToRay(Input.mousePosition), out hit, 10000, GroundMask))
        {
            print("Summon");
            projectile.portelAnimation();            
            //LeanTween.delayedCall(0.55f, () => { isPortelOpen = false; });
            isPortelOpen = true;

            //LeanTween.delayedCall(0.02f, () => {  });                        
            //LeanTween.delayedCall(0.02f, () => { vfx_Portel.Play(); });                        
            //Debug.Log(hit.point);
        }
        
    }

    public void SummonPortel()
    {
        projectile.Portel(hit.point + new Vector3(0, 0.1f, 0));
    }
    void mouseCheck()
    {    mousePos = Input.mousePosition;
        mousePos.z = 1000f;
        mousePos = Cam.ScreenToWorldPoint(mousePos);
        Debug.DrawRay(transform.position, mousePos - transform.position, Color.red);
    }
}
