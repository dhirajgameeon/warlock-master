using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class LookAt : MonoBehaviour
{
    public CustomAttributes PlayerStat;
    public GameObject[] collidingObjects;
    public Collider[] CollideNearObject;
    public LayerMask Priest;
    public GameObject PriestToLookAt;
    public float delayRotation = 0.15f;
    [SerializeField] int countRotation;
    private movePlayer move;

    void Awake()
    {
        move = GetComponent<movePlayer>();   
    }


    void Update()
    {
        CollideNearObject = Physics.OverlapSphere(transform.position, (float)PlayerStat.checkingRedius, Priest);
        checkForPriest();
        managerPriest();
        moveForward();
        if (PlayerStat.HP <= 0 && !GameManager.instance.isPlayerDied)
        {
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PlayerDeath);
            GameManager.instance.isPlayerDied = true;
        }
    }
    private void LateUpdate()
    {
        //if (!move.isMoving) LookAtPriest();
    }

    void checkForPriest()
    {
        collidingObjects = GameObject.FindGameObjectsWithTag("Priest");
        float closestDistance = Mathf.Infinity;
        PriestToLookAt = null;

        foreach (GameObject item in collidingObjects)
        {
            if (!item.GetComponent<controlPriest>().isDead)
            {
                float currentDiatance;
                currentDiatance = Vector3.Distance(transform.position, item.transform.position);
                if (currentDiatance < closestDistance)
                {
                    closestDistance = currentDiatance;                    
                    if (!move.isMoving) PriestToLookAt = item;
                }
            }
        }
    }

    void moveForward()
    {
        if (CollideNearObject.Length <= 0)
        {
            if (move.waveNumber == 1 && !WaveManager.wave.CompleteWave[0] && !move.isMove && !move.isMoving)
            {
                move.movePosition[0] = new Vector3(0, 0f, move.movePosition[0].z + 5);
                move.nextPosition = move.movePosition[0];
                move.isMove = true;
                move.isMidMove = true;
            }
            if (move.waveNumber == 2 && !WaveManager.wave.CompleteWave[1]  && !move.isMove && !move.isMoving)
            {
                move.movePosition[1] = new Vector3(0, 1.8f, move.movePosition[1].z + 5);
                move.nextPosition = move.movePosition[1];
                move.isMove = true;
                move.isMidMove = true;
            }
            if (move.waveNumber == 3&& !WaveManager.wave.CompleteWave[2] && !move.isMove && !move.isMoving)
            {
                move.movePosition[2] = new Vector3(0, 3.6f, move.movePosition[2].z + 5);
                move.nextPosition = move.movePosition[2];
                move.isMove = true;
                move.isMidMove = true;
            }
        }
            //print("Null");
    }
    void managerPriest()
    {
        if (PriestToLookAt)
        {
            if (PriestToLookAt.GetComponent<controlPriest>().isDead)
            {
                PriestToLookAt = null;
            }
        }
    }
    void LookAtPriest()
    {
        if (PriestToLookAt)
        {
            smoothLookat(PriestToLookAt.transform.position, PlayerStat.TurnRate);            
        }
        if(!PriestToLookAt)
        {
            //smoothLookat(Vector3.zero, PlayerLookAt.TurnRate / 5);
        }
    }

    float y = 0.15f;
    void smoothLookat(Vector3 target, float rotationSpeed)
    {
        var targetPos = target;
        targetPos.y = transform.position.y;
        var targetDir = Quaternion.LookRotation(targetPos - transform.position);
        if(transform.rotation != targetDir )
        {
            rotator(delayRotation, targetDir, rotationSpeed);
            //transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, rotationSpeed * Time.deltaTime);
        }      
    }
    void rotator(float t, Quaternion targetDir, float rotationSpeed)
    {
        if (y > 0) y -= Time.deltaTime;
        if (y <= 0)
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, rotationSpeed * Time.deltaTime);
            if (transform.rotation == targetDir) 
            { 
                y = delayRotation; 
            }
        }

       
        
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Handles.color = PlayerStat.checkingColor;
        Handles.DrawWireDisc(transform.position, Vector3.up, PlayerStat.checkingRedius, 1);
    }
#endif
}
