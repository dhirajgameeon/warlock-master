using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class impact : MonoBehaviour
{
    public float damage = 0;
    public int damageType = 0;
    public float delay = 0.15f;
    public float destroyDelay = 0.5f;
    public bool isCollideWithPlayer;
    private Transform player;
    void Start()
    {
        player = GameObject.Find("Player").transform;
        LeanTween.delayedCall(destroyDelay, () => { Destroy(this.gameObject); });
    }

    // Update is called once per frame
    void Update()
    {
        missSFX();
    }

    void missSFX()
    {
        if (Vector3.Distance(transform.position, player.position) <= 2 && !isCollideWithPlayer)
        {
            EvenManager.TriggerSFXOneShotPlayEvent(AudioID.PlayerProjectileMiss);
            isCollideWithPlayer = true;
        }
    }

    private void OnCollisionEnter(Collision other)
    {
        isCollideWithPlayer = true;
        try
        {
            TakeDamageType TDT = other.gameObject.GetComponent<TakeDamageType>();
           if(TDT.DamageType == collisionType.head)other.gameObject.GetComponent<IDamageable>()?.TakeDamage(collisionType.head);
           if(TDT.DamageType == collisionType.body)other.gameObject.GetComponent<IDamageable>()?.TakeDamage(collisionType.body);
           if(TDT.DamageType == collisionType.legs)other.gameObject.GetComponent<IDamageable>()?.TakeDamage(collisionType.legs);
        }
        catch
        {
            //other.gameObject.GetComponent<IDamageable>()?.TakeDamage(collisionType.legs);
        }
        try
        {
            other.gameObject.GetComponent<LookAt>().PlayerStat.HP -= damage;
        }
        catch
        {

        }
    }
}
