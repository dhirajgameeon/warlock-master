using UnityEngine;

public class Tentacle : MonoBehaviour
{
    public Animator anim;
    public TakeDamageType TDT;
    public float attackDelay = 1;
    void Start()
    {
        anim.SetInteger("Type", Random.Range(0, 2));
        anim.SetFloat("speed", Random.Range(1.5f, 3));
        /*LeanTween.delayedCall(attackDelay, () => { takeDamage(); });*/
    }

   


    public void takeDamage()
    {
        TDT.TakeDamage(collisionType.head, 10, 1);
        LeanTween.delayedCall(attackDelay, () => { TDT.TakeDamage(collisionType.head, 200); });
    }
}
