using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeDemonMove : MonoBehaviour
{
    public List<TakeDamageType> bodyPart = new List<TakeDamageType>();
    public GameObject[] Tentacle;
    public float AreaOfDetection = 20;
    public float slowMotionDealy = 0.5f;
    public Collider[] Head;
    public LayerMask HeadLayer;
    public int NeedSoul;
    public bool isTentacles;
    public bool isEyeOpen;
    public bool destroy;

    private Transform Player;
    private void Awake()
    {
        Player = GameObject.Find("Player").transform;
    }
    void Start()
    {
        if (!isTentacles) EyeMove();
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.EyeDemon);
    }

    void slwM()
    {
        Time.timeScale = 0.7f;
        LeanTween.delayedCall(slowMotionDealy, () => { Time.timeScale = 1; });
    }

    // Update is called once per frame
    void Update()
    {
      /*  bodyPart = PriestManager.instance.Head;*/
        if(!GameManager.instance.isGameComplete && !GameManager.instance.isPlayerDied) TentaclesMove();
        checkForHeadNear();
    }

    void checkForHeadNear()
    {
        Head = Physics.OverlapSphere(transform.position, AreaOfDetection, HeadLayer);
        foreach (Collider item in Head)
        {
            if(item.GetComponent<TakeDamageType>().DamageType == collisionType.head && !bodyPart.Contains(item.GetComponent<TakeDamageType>()))
            {
                bodyPart.Add(item.GetComponent<TakeDamageType>());
            }
        }
        destroy = bodyPart.TrueForAll(bodyPart => bodyPart.isDead == true);

        if (destroy) Destroy(this.gameObject,2);
    }

    public void EyeMove()
    {
        LeanTween.move(gameObject, new Vector3(transform.position.x, Player.position.y, transform.position.z), 0.5f).setOnComplete(() => {
            isEyeOpen = true;
            slwM();
        });
        if (GameManager.instance.SoulCount > 0) 
        {

            GameManager.instance.SoulCount -= NeedSoul; 
            //PlayerPrefs.SetInt("TotalSoul", GameManager.instance.SC);
        }
    }
    public void TentaclesMove()
    {
        if (isEyeOpen)
        {
            for (int i = 0; i <= bodyPart.Count-1; i++)
            {
                GameObject Tent = Instantiate(Tentacle[Random.Range(0, 2)], transform);
                Tent.transform.position = new Vector3(bodyPart[i].transform.position.x + 1, transform.position.y - 0.3f, bodyPart[i].transform.position.z);
                Tent.GetComponent<Tentacle>().TDT = bodyPart[i];

                if (i >= bodyPart.Count - 1) isEyeOpen = false;
            }
        }
    }
}
