using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class movePriest : MonoBehaviour
{
    public List<controlPriest> armyPrist = new List<controlPriest>();
    public AnimationClip[] hitReact;

    public GameObject UI;
    public GameObject inactiveMesh;
    public GameObject activeMesh;
    public bool isActive = false;
    public bool isLeaderPriest = false;
    public bool isArmyDead = false;
    public bool isAttacking = false;
    public bool isMoving = false;
    public bool isStopped = false;
    public float stoppingDistance = 5f;
    public Vector2 RangeDistance;
    public float rotationSmooth = 0.2f;
    private float turnSmoothVelocity;

    [HideInInspector]public NavMeshAgent agent;
    [HideInInspector]public controlPriest controlPriest;
    private AnimationHandler anim;
    private PLookAt priest;
    attackPriest atkP;


    void Awake()
    {
        controlPriest = GetComponent<controlPriest>();
        anim = GetComponent<AnimationHandler>();
        priest = GetComponent<PLookAt>();
        agent = GetComponent<NavMeshAgent>();
        atkP = GetComponent<attackPriest>();
    }

    private void Start()
    {
        stoppingDistance = Random.Range(RangeDistance.x, RangeDistance.y);
        agent.stoppingDistance = stoppingDistance;
        agent.speed = priest.PriestStat.MovementSpeed;
    }

    void showInactiveAgent()
    {
        if (!isActive){
            inactiveMesh.SetActive(true); 
            activeMesh.SetActive(false); 
        } 
        else if(isActive) { 
            activeMesh.SetActive(true);
            inactiveMesh.SetActive(false);
        }
    }
    void Update()
    {
        showInactiveAgent();
        todoWhenActive();
        /*if (controlPriest.isStunned && !isAttacking) agent.isStopped = true; else { agent.isStopped = false; }*/

        if (!isAttacking) AgentMovement();
       
    }

    void todoWhenActive()
    {
        if(isLeaderPriest) isArmyDead = armyPrist.TrueForAll(armyPrist => armyPrist.isDead == true);

        if (isActive)
        {
            anim.anim.SetBool("isStun", controlPriest.isStunned);
            UI.SetActive(isAttacking || controlPriest.isStunned);

            if (!isLeaderPriest)
            {
                if (!controlPriest.isStunned && !priest.Player.GetComponent<movePlayer>().isMoving) move();
                if (controlPriest.isStunned) isMoving = !controlPriest.isStunned;
            }
            if (isLeaderPriest)
            {
                /* if (isAttacking)
                 {
                     smoothLookat(atkP.target.position, priest.PriestStat.TurnRate);
                 }*/

                if (!controlPriest.isStunned && !priest.Player.GetComponent<movePlayer>().isMoving) moveIfLeader();
                if (controlPriest.isStunned) isMoving = !controlPriest.isStunned;
                
            }
        }
        if (!GameManager.instance.isPlayerDied) agent.isStopped = !isMoving;
    }

    void move()
    {
      /*  agent.SetDestination(atkP.target.position);
        agent.isStopped = isAttacking;

        if (agent.velocity.magnitude > 0f)
        {
            isMoving = true;
        }
        if (agent.velocity.magnitude < 0.1f *//*&& Vector3.Distance(transform.position, atkP.target.position) < agent.stoppingDistance*//*)
        {
            isMoving = false;
        }
*/
        if (!GameManager.instance.isPlayerDied && !isAttacking && Vector3.Distance(transform.position, atkP.target.position) > stoppingDistance)
        {
            agent.SetDestination(atkP.target.position);
            isMoving = true;
        }
        else
        {
            isMoving = false;
        }

        if (GameManager.instance.isPlayerDied)
        {
            agent.isStopped = GameManager.instance.isPlayerDied;
        }
        /*agent.isStopped = isAttacking;*/
    }

   
    void moveIfLeader()
    {

            if (!isAttacking && Vector3.Distance(transform.position, atkP.target.position) > stoppingDistance)
            {                
                agent.SetDestination(atkP.target.position);
                isMoving = true;
            }
            else
            {
                isMoving = false;
            }
            /*agent.isStopped = isAttacking;*/
    }

/*    void smoothLookat(Vector3 target, float rotationSpeed)
    {
        var targetPos = target;
        targetPos.y = transform.position.y;
        var targetDir = Quaternion.LookRotation(targetPos - transform.position);
    }*/
    void AgentMovement()
    {
        if (agent.velocity.magnitude > 0.1f)
        {
            float targetAngle = Mathf.Atan2(agent.velocity.x, agent.velocity.z) * Mathf.Rad2Deg;
            float angle = Mathf.SmoothDampAngle(transform.eulerAngles.y, targetAngle, ref turnSmoothVelocity, rotationSmooth);
            transform.rotation = Quaternion.Euler(0, angle, 0);
        }
    }
}
