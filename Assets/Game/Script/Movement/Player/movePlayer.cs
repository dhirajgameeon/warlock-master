using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePlayer : MonoBehaviour
{
    public Vector3[] movePosition;
    public Animator anim;
    public ParticleSystem Speedline;
    public Vector3 FinalLookAt;

    public float moveSpeed = 7;
    public float delayMove = 1;

    public bool isMidMove = false;
    public bool isMove = false;
    public bool isMoving = false;

    private LookAt PlayerLookAt;
    [SerializeField]private Vector3 currentPosition;
    public Vector3 nextPosition;
    public int waveNumber;

    private void Awake()
    {
        PlayerLookAt = GetComponent<LookAt>();
        
    }
    void Start()
    {
        GameManager.instance.isGameRunning = true;
        nextPosition = movePosition[waveNumber];
        move();
    }

    // Update is called once per frame
    void Update()
    {
        //onGameOver();

        if (isMoving && !GameManager.instance.isGameComplete) Speedline.Play();
        if (!isMoving) Speedline.Stop();

        if (isMove) move();
        if (isMoving && waveNumber != 4) smoothLookat(currentPosition, PlayerLookAt.PlayerStat.TurnRate);

        anim.SetBool("isRun", isMoving);

    }

    float x = 0;
    public void move()
    {
        if (x > 0) x -= Time.deltaTime;
        if (x <= 0)
        {
            if (currentPosition != nextPosition)
            {
                if (!isMidMove)
                {
                    currentPosition = nextPosition;
                    isMoving = true;
                    GameManager.instance.isGameRunning = false;
                    LeanTween.move(gameObject, currentPosition, PlayerLookAt.PlayerStat.MovementSpeed).setOnComplete(() => {
                        x = delayMove;
                        isMoving = false;
                        GameManager.instance.isGameRunning = true;
                    });
                    waveNumber += 1;
                    if (waveNumber < movePosition.Length) nextPosition = movePosition[waveNumber];
                    else { print("<color=red>Move ID is greater than Move Position List</color>"); }
                    PlayerLookAt.PlayerStat.MovementSpeed = moveSpeed;
                    isMove = false;
                }
                if (isMidMove)
                {
                    currentPosition = nextPosition;
                    isMoving = true;
                    GameManager.instance.isGameRunning = false;
                    LeanTween.move(gameObject, currentPosition, PlayerLookAt.PlayerStat.MovementSpeed/2).setOnComplete(() => {
                        x = delayMove;
                        isMoving = false;
                        isMidMove = false;
                        if (waveNumber < movePosition.Length) nextPosition = movePosition[waveNumber ];
                        GameManager.instance.isGameRunning = true;
                    });
                   /* if (waveNumber < movePosition.Length) nextPosition = movePosition[waveNumber + 1];
                    else { print("<color=red>Move ID is greater than Move Position List</color>"); }*/
                    PlayerLookAt.PlayerStat.MovementSpeed = moveSpeed;
                    
                    isMove = false;
                }                
            }
        }
        
    }

    void onGameOver()
    {
        if (GameManager.instance.isGameComplete)
        {
            smoothLookat(FinalLookAt, PlayerLookAt.PlayerStat.TurnRate / 3);
        }
    }
    void smoothLookat(Vector3 target, float rotationSpeed)
    {
        var targetPos = target;
        targetPos.y = transform.position.y;
        var targetDir = Quaternion.LookRotation(targetPos - transform.position);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetDir, rotationSpeed * Time.deltaTime);
    }
}
