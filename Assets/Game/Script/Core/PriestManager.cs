using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PriestManager : MonoBehaviour
{
    public static PriestManager instance;

    private void Awake()
    {
        instance = this;
    }

    public List<TakeDamageType> Legs, Head, Body = new List<TakeDamageType>();
    private TakeDamageType[] TDT;

    void Update()
    {
        checkForBodyPart();
        ResetBodyPart();
    }
    void checkForBodyPart()
    {
        TDT = FindObjectsOfType<TakeDamageType>();
        foreach (TakeDamageType TD in TDT)
        {
            if(!TD.DoNotInclude && TD.boneRender.activeSelf && !TD.isDead)
            {
                if (collisionType.legs == TD.DamageType && !Legs.Contains(TD)) Legs.Add(TD);
                if (collisionType.body == TD.DamageType && !Body.Contains(TD)) Body.Add(TD);
                if (collisionType.head == TD.DamageType && !Head.Contains(TD)) Head.Add(TD);
            }
        }
    }
    private void ResetBodyPart()
    {
        foreach(TakeDamageType TD in Legs.ToArray())
        {
            if(!TD.boneRender.activeSelf  || TD.isDead && Legs.Contains(TD))Legs.Remove(TD);
        }
        foreach (TakeDamageType TD in Head.ToArray())
        {
            if (!TD.boneRender.activeSelf || TD.isDead && Head.Contains(TD)) Head.Remove(TD);
        }
        foreach (TakeDamageType TD in Body.ToArray())
        {
            if (!TD.boneRender.activeSelf || TD.isDead && Body.Contains(TD)) Body.Remove(TD);
        }
    }
}
