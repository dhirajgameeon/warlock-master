using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DemoPlayAudio : MonoBehaviour
{
    void Start()
    {
        BGMplay();
    }

    void BGMplay()
    {
        EvenManager.TriggerBGMPlayEvent(AudioID.BGM);
    }
}
