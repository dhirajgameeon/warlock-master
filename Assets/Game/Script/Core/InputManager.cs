using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.InputSystem;
public class InputManager : MonoBehaviour
{
    public Image image;
    public TMP_Text text;
    Vector2 touchPos;

    private void Update()
    {

        text.text = touchPos.ToString();
    }
    public void OnTouch(InputAction.CallbackContext context)
    {
        if (context.started) { image.gameObject.SetActive(true); }

        if (context.canceled) { image.gameObject.SetActive(false); }
    }
    public void OnTouchPositionMove(InputAction.CallbackContext value)
    {
        touchPos = value.ReadValue<Vector2>();
    }
}
