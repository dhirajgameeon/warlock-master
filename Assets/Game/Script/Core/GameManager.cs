using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;


public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    private void Awake()
    {
        instance = this;
    }
    public int LevelCount = 0;
    public int Level = 0;

    [Header("UIs")]
    [Space(5)]
    public TMP_Text TLevelCount;
    public TMP_Text TSoulCount;
    public GameObject GameOverUI;
    public Image circler;
    public GameObject SummonUI;
    public Image SummenType;

    /// <summary>
    /// Cash Counter
    /// </summary>

    [Header("Cash Counter")]
    public float maxCash;
    public float cashCounterSpeed;
    private float currentCash;
    public TextMeshProUGUI TCoinCount;

    /// <summary>
    /// Progress Slider
    /// </summary>
    [Header("Slider")]
    [Space(10)]
    public Slider customerCount;
    public float USpeed;
    public float DSpeed;

    public float maxWave = 3;
    public float CompleteWave = 0;
    private float currentWave = 0;

    [Header("Level Complete Action")]
    [Space(5)]
    public GameObject Confetti;
    public GameObject UI;
    public float ConfettiDelay = 2.5f;

    [Space(5)]
    public bool isGameComplete = false;
    public bool isGameOver = false;
    public bool isGameRunning = false;
    public bool isPlayerDied = false;

    public int SoulCount = 0;
    public Animator SoulAnimation;
    public GameObject[] summons;
    public Sprite[] SummonT;



    void Start()
    {
        customerCount.maxValue = maxWave;

        //SC = PlayerPrefs.GetInt("TotalSoul");
        maxCash = PlayerPrefs.GetFloat("Coin");
        currentCash = maxCash;
    }

    
    void Update()
    {
        customerCount.value = progressSlider(CompleteWave);
        SoulCount = Mathf.Clamp(SoulCount, 0, 5);
        UIManage();
        GameOver();
        confetti();
        summonUIUpdate();
        if (Input.GetKeyDown(KeyCode.R))
        {
            resetCount();
            Retry();
        }


    }

    public void summonUIUpdate()
    {
        if(SoulCount>0&& SoulCount <= 2)
        {
            SummenType.sprite = SummonT[0];
        }
        if (SoulCount == 3)
        {
            SummenType.sprite = SummonT[1];
        }
        if (SoulCount == 4)
        {
            SummenType.sprite = SummonT[2];
        }
        if (SoulCount == 5)
        {
            SummenType.sprite = SummonT[3];
        }
    }
    public void resetCount()
    {
        PlayerPrefs.SetInt("TotalSoul",0);
        PlayerPrefs.SetFloat("Coin", 0);
    }
    public void confetti()
    {
        if (isGameComplete && !isGameOver)
        {
            CompleteWave = maxWave;
            StartCoroutine(GameOver(ConfettiDelay));
            isGameOver = true;
        }
    }
    IEnumerator GameOver(float t)
    {
        yield return new WaitForSeconds(t);
        EvenManager.TriggerSFXOneShotPlayEvent(AudioID.LevelComplete);
        Confetti.SetActive(true);
        UI.SetActive(true);
    }

    public void Retry()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void NextLevel()
    {
        SceneManager.LoadScene(LevelCount + 1);
    }

    void UIManage()
    {
        TLevelCount.text = "Level " + Level;
        TCoinCount.text = cashCount().ToString("0");
        TSoulCount.text = SoulCount.ToString("0"); 
    }

    float cashCount()
    {
        currentCash = Mathf.Clamp(currentCash, 0, Mathf.Infinity);
        if (currentCash <= maxCash)
        {
            currentCash += cashCounterSpeed * Time.deltaTime;
            if (currentCash >= maxCash) currentCash = maxCash;
        }
        if (currentCash >= maxCash)
        {
            currentCash -= cashCounterSpeed * Time.deltaTime;
            if (currentCash <= maxCash) currentCash = maxCash;
        }
        if (currentCash < 0 || maxCash < 0)
        {
            currentCash = 0;
            maxCash = 0;
        }
        return currentCash;

    }

    void GameOver()
    {
        if (isPlayerDied) GameOverUI.SetActive(true);
    }

    float currentProgress;
    float progressSlider(float maxProgress)
    {
        if (currentProgress < maxProgress)
        {
            currentProgress += USpeed * Time.deltaTime;
            if (currentProgress >= maxProgress) currentProgress = maxProgress;
        }
        if (currentProgress > maxProgress)
        {
            currentProgress -= DSpeed * Time.deltaTime;
            if (currentProgress <= maxProgress) currentProgress = maxProgress;
        }
        if (currentProgress == maxProgress)
            currentProgress = maxProgress;

        return currentProgress;
    }
}
