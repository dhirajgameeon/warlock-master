using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialManager : MonoBehaviour
{
    public static TutorialManager instance;

    public int SoulCount;
    public float TapDelay = 5;
    public Animator TapAndHold;
    public Animator TapUI;
    private movePlayer MovePlayer;
    private PlayerPrefs Soul;

    public bool isTapped = false;
    public bool isHolding = false;
    public bool isHolding1 = false;
    public bool isHolding2 = false;
    private void Awake()
    {
        instance = this;
        MovePlayer = FindObjectOfType<movePlayer>();
    }

    private void Start()
    {
        PlayerPrefs.SetInt("soul", 0);
        tap = TapDelay;
    }

    private void Update()
    {
        if (!MovePlayer.isMove&& !MovePlayer.isMoving)
        {            
            Hold();
        }
        if (MovePlayer.isMoving) TapAndHold.gameObject.SetActive(false);
        Tap();

        SoulCount = PlayerPrefs.GetInt("soul");
    }

    void Hold()
    {
        if (!isHolding && SoulCount == 1 ) { TapAndHold.gameObject.SetActive(true); TapAndHold.Play("Tap And Hold");  }
        if (!isHolding1 && SoulCount == 3 ) { TapAndHold.gameObject.SetActive(true); TapAndHold.Play("Tap And Hold"); }
        if (!isHolding2 && SoulCount == 5) { TapAndHold.gameObject.SetActive(true); TapAndHold.Play("Tap And Hold"); }

    }

    float tap = 0;

    void Tap()
    {
        if (tap > 0 && !isTapped) tap -= Time.deltaTime;
        if(tap<=0 && !isTapped)
        {
            if (MovePlayer.waveNumber == 1 && !MovePlayer.isMoving) TapUI.Play("Tap");
        }

        if (Input.GetMouseButtonDown(0)) { 
            
            isTapped = true;
            TapUI.gameObject.SetActive(false);
        }
    }
}
