using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public List<controlPriest> Wave_1 = new List<controlPriest>();
    public List<controlPriest> Wave_2 = new List<controlPriest>();
    public List<controlPriest> Wave_3 = new List<controlPriest>();

    public List<bool>CompleteWave=new List<bool>();

    public List<Transform> Waves = new List<Transform>();

    public List<controlPriest> HPriest = new List<controlPriest>();

    public bool isAllHpriestDied = false;
    private movePlayer movePlayer;
    private LookAt PlayerLooking;

    public static WaveManager wave;

    private void Awake()
    {
        wave = this;
        movePlayer = FindObjectOfType<movePlayer>();
        PlayerLooking = FindObjectOfType<LookAt>();
    }

    private void Update()
    {
        waveChecker();
    }
    void waveChecker()
    {
       if(movePlayer.waveNumber == 1 && !movePlayer.isMidMove)
        {            
            movePlayer.isMove = Wave_1.TrueForAll(Wave_1 => Wave_1.isDead == true);
        }
        if (movePlayer.waveNumber == 2 && !movePlayer.isMidMove)
        {
            movePlayer.isMove = Wave_2.TrueForAll(Wave_2 => Wave_2.isDead == true);
        }
        if (movePlayer.waveNumber == 3 && !movePlayer.isMidMove)
        {
            movePlayer.isMove = Wave_3.TrueForAll(Wave_3 => Wave_3.isDead == true);
            GameManager.instance.isGameComplete = Wave_3.TrueForAll(Wave_3 => Wave_3.isDead == true);
        }

        if(Wave_1.TrueForAll(Wave_1 => Wave_1.isDead == true))
        {
            if (!movePlayer.isMove && !CompleteWave[0])
            {


                foreach (controlPriest item in Wave_2)
                {

                    if (!item.move.isActive)
                    {
  
                        item.tag = "Priest";
                        item.move.isActive = true;


                    }
                }
                CompleteWave[0] = true;
                GameManager.instance.CompleteWave = 1;
            }
        }
        if (Wave_2.TrueForAll(Wave_2 => Wave_2.isDead == true))
        {
            if (!movePlayer.isMove &&  !CompleteWave[1])
            {
                foreach (controlPriest item in Wave_3)
                {
                    if (!item.move.isActive)
                    {
                        item.tag = "Priest";
                        item.move.isActive = true;
                    }
                }
                CompleteWave[1] = true;
                GameManager.instance.CompleteWave = 2;
            }
        }
        if (Wave_3.TrueForAll(Wave_3 => Wave_3.isDead == true))
        {
            GameManager.instance.CompleteWave = 3;
            CompleteWave[2] = true;
        }

        isAllHpriestDied = HPriest.TrueForAll(HPriest => HPriest.isDead == true);
    }
}
