using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamageFromEnemy : MonoBehaviour, IDamageable
{
    public collisionType DamageType;
    public LookAt Player;
    private void Awake()
    {
        Player = GetComponent<LookAt>();
    }

    public void TakeDamage(collisionType damageType)
    {
        switch (damageType)
        {
            case collisionType.body:
                Player.PlayerStat.HP -= Player.PlayerStat.BodyDamage;                
                break;
            case collisionType.head:
                Player.PlayerStat.HP -= Player.PlayerStat.HeadDamage;                
                break;
            case collisionType.legs:
                Player.PlayerStat.HP -= Player.PlayerStat.LegDamage;                
                break;

        }
    }
    public void TakeDamage(collisionType damageType, float damage, int i = 0)
    {

    }
}
