using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPlayer : MonoBehaviour
{
    public ParticleSystem VFX;

    private controlRaycast cR;
    private void Awake()
    {
        cR = FindObjectOfType<controlRaycast>();
    }
    public void PlayVFX()
    {
        VFX.Play();
    }
    public void summonPortel()
    {
        cR.SummonPortel();
    }

}
