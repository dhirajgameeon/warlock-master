using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleHit : MonoBehaviour
{
    public Tentacle T;

    public void TakeHit()
    {
        T.takeDamage();
    }
}
