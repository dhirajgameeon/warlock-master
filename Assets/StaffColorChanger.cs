using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaffColorChanger : MonoBehaviour
{
    private GameManager gameManager;
    public int SC;

    [Header("Staff Ref")]
    [Space(5)]
    public List<MeshRenderer> Teeth = new List<MeshRenderer>();
    public MeshRenderer Body;

    [Header("Color Ref")]
    [Space(5)]
    [ColorUsageAttribute(true, true)] public Color TeethInitialColor;
    [ColorUsageAttribute(true, true)] public Color TeethChangeColor_0;
    [ColorUsageAttribute(true, true)] public Color TeethChangeColor;

    [Space(3)]
    public Color BodyInitCol;
    [ColorUsageAttribute(true, true)] public Color BodyChangeCol_0;
    [ColorUsageAttribute(true, true)] public Color BodyChangeCol;

    [Space(3)]
    public Color EyeInitColor;
    [ColorUsageAttribute(true, true)] public Color Eye_1_0;
    [ColorUsageAttribute(true, true)] public Color Eye_1;
    [ColorUsageAttribute(true, true)] public Color Eye_2_0;
    [ColorUsageAttribute(true, true)] public Color Eye_2;
    [ColorUsageAttribute(true, true)] public Color Eye_3_0;
    [ColorUsageAttribute(true, true)] public Color Eye_3;

    public float lerpSpeed;
    void Start()
    {
        gameManager = GameManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        SC = gameManager.SoulCount;

        StaffModifier();
    }

    [Space(10)]
    public float duration;
    void changeColorS(Material mr, Color col1, Color col2, float duration)
    {
        float phi = (Time.time / duration) * 2 * Mathf.PI;
        float altitude = Mathf.Cos(phi) * 0.5f + 0.5f;
        mr.color = Color.Lerp(col1, col2, altitude);
    }
    void StaffModifier()
    {
        if (SC == 0)
        {
            //Teeth
            changeColorS(Teeth[0].material, Teeth[0].material.color, TeethInitialColor, duration);
            changeColorS(Teeth[1].material, Teeth[1].material.color, TeethInitialColor, duration);
            changeColorS(Teeth[2].material, Teeth[2].material.color, TeethInitialColor, duration);
            changeColorS(Teeth[3].material, Teeth[3].material.color, TeethInitialColor, duration);

            /*
                        Teeth[0].material.color = Color.Lerp(Teeth[0].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[1].material.color = Color.Lerp(Teeth[1].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[2].material.color = Color.Lerp(Teeth[2].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[3].material.color = Color.Lerp(Teeth[3].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);*/

            //Body

            changeColorS(Body.materials[0], Body.materials[0].color, BodyInitCol, duration);
            changeColorS(Body.materials[1], Body.materials[1].color, BodyInitCol, duration);

/*            Body.materials[0].color = Color.Lerp(Body.materials[0].color, BodyInitCol, lerpSpeed * Time.deltaTime); 
            Body.materials[1].color = Color.Lerp(Body.materials[1].color, BodyInitCol, lerpSpeed * Time.deltaTime);*/

            //Eyes
            changeColorS(Body.materials[2], Body.materials[2].color, EyeInitColor, duration);
            /*Body.materials[2].color = Color.Lerp(Body.materials[2].color, EyeInitColor, lerpSpeed * Time.deltaTime);*/
        }
        if (SC == 1)
        {
            //Teeth
            changeColorS(Teeth[0].material, TeethChangeColor, TeethChangeColor_0, duration);
            changeColorS(Teeth[1].material, Teeth[1].material.color, TeethInitialColor, duration);
            changeColorS(Teeth[2].material, Teeth[2].material.color, TeethInitialColor, duration);
            changeColorS(Teeth[3].material, Teeth[3].material.color, TeethInitialColor, duration);

            /*
                        Teeth[0].material.color = Color.Lerp(Teeth[0].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[1].material.color = Color.Lerp(Teeth[1].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[2].material.color = Color.Lerp(Teeth[2].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[3].material.color = Color.Lerp(Teeth[3].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);*/

            //Body

            changeColorS(Body.materials[0], Body.materials[0].color, BodyInitCol, duration);
            changeColorS(Body.materials[1], Body.materials[1].color, BodyInitCol, duration);

            /*            Body.materials[0].color = Color.Lerp(Body.materials[0].color, BodyInitCol, lerpSpeed * Time.deltaTime); 
                        Body.materials[1].color = Color.Lerp(Body.materials[1].color, BodyInitCol, lerpSpeed * Time.deltaTime);*/

            //Eyes
            changeColorS(Body.materials[2], Eye_1, Eye_1_0, duration);
            /*Body.materials[2].color = Color.Lerp(Body.materials[2].color, EyeInitColor, lerpSpeed * Time.deltaTime);*/
        }
        if (SC == 2)
        {
            //Teeth
            changeColorS(Teeth[0].material, TeethChangeColor, TeethChangeColor_0, duration);
            changeColorS(Teeth[1].material, TeethChangeColor, TeethChangeColor_0, duration);
            
            changeColorS(Teeth[2].material, Teeth[2].material.color, TeethInitialColor, duration);
            changeColorS(Teeth[3].material, Teeth[3].material.color, TeethInitialColor, duration);

            /*
                        Teeth[0].material.color = Color.Lerp(Teeth[0].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[1].material.color = Color.Lerp(Teeth[1].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[2].material.color = Color.Lerp(Teeth[2].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[3].material.color = Color.Lerp(Teeth[3].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);*/

            //Body

            changeColorS(Body.materials[0], Body.materials[0].color, BodyInitCol, duration);
            changeColorS(Body.materials[1], Body.materials[1].color, BodyInitCol, duration);

            /*            Body.materials[0].color = Color.Lerp(Body.materials[0].color, BodyInitCol, lerpSpeed * Time.deltaTime); 
                        Body.materials[1].color = Color.Lerp(Body.materials[1].color, BodyInitCol, lerpSpeed * Time.deltaTime);*/

            //Eyes
            changeColorS(Body.materials[2], Eye_1, Eye_1_0, duration);
            /*Body.materials[2].color = Color.Lerp(Body.materials[2].color, EyeInitColor, lerpSpeed * Time.deltaTime);*/
        }
        if (SC == 3)
        {
            //Teeth
            changeColorS(Teeth[0].material, TeethChangeColor, TeethChangeColor_0, duration);
            changeColorS(Teeth[1].material, TeethChangeColor, TeethChangeColor_0, duration);
            changeColorS(Teeth[2].material, TeethChangeColor, TeethChangeColor_0, duration);

            changeColorS(Teeth[3].material, Teeth[3].material.color, TeethInitialColor, duration);

            /*
                        Teeth[0].material.color = Color.Lerp(Teeth[0].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[1].material.color = Color.Lerp(Teeth[1].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[2].material.color = Color.Lerp(Teeth[2].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[3].material.color = Color.Lerp(Teeth[3].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);*/

            //Body

            changeColorS(Body.materials[0], Body.materials[0].color, BodyInitCol, duration);
            changeColorS(Body.materials[1], Body.materials[1].color, BodyInitCol, duration);

            /*            Body.materials[0].color = Color.Lerp(Body.materials[0].color, BodyInitCol, lerpSpeed * Time.deltaTime); 
                        Body.materials[1].color = Color.Lerp(Body.materials[1].color, BodyInitCol, lerpSpeed * Time.deltaTime);*/

            //Eyes
            changeColorS(Body.materials[2], Eye_2, Eye_2_0, duration);
            /*Body.materials[2].color = Color.Lerp(Body.materials[2].color, EyeInitColor, lerpSpeed * Time.deltaTime);*/
        }
        if (SC == 4)
        {
            //Teeth
            changeColorS(Teeth[0].material, TeethChangeColor, TeethChangeColor_0, duration);
            changeColorS(Teeth[1].material, TeethChangeColor, TeethChangeColor_0, duration);
            changeColorS(Teeth[2].material, TeethChangeColor, TeethChangeColor_0, duration);
            changeColorS(Teeth[3].material, TeethChangeColor, TeethChangeColor_0, duration);


            /*
                        Teeth[0].material.color = Color.Lerp(Teeth[0].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[1].material.color = Color.Lerp(Teeth[1].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[2].material.color = Color.Lerp(Teeth[2].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[3].material.color = Color.Lerp(Teeth[3].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);*/

            //Body

            changeColorS(Body.materials[0], Body.materials[0].color, BodyInitCol, duration);
            changeColorS(Body.materials[1], Body.materials[1].color, BodyInitCol, duration);

            /*            Body.materials[0].color = Color.Lerp(Body.materials[0].color, BodyInitCol, lerpSpeed * Time.deltaTime); 
                        Body.materials[1].color = Color.Lerp(Body.materials[1].color, BodyInitCol, lerpSpeed * Time.deltaTime);*/

            //Eyes
            changeColorS(Body.materials[2], Eye_2, Eye_2_0, duration);
            /*Body.materials[2].color = Color.Lerp(Body.materials[2].color, EyeInitColor, lerpSpeed * Time.deltaTime);*/
        }
        if (SC == 5)
        {
            //Teeth
            changeColorS(Teeth[0].material, TeethChangeColor, TeethChangeColor_0, duration);
            changeColorS(Teeth[1].material, TeethChangeColor, TeethChangeColor_0, duration);
            changeColorS(Teeth[2].material, TeethChangeColor, TeethChangeColor_0, duration);
            changeColorS(Teeth[3].material, TeethChangeColor, TeethChangeColor_0, duration);


            /*
                        Teeth[0].material.color = Color.Lerp(Teeth[0].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[1].material.color = Color.Lerp(Teeth[1].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[2].material.color = Color.Lerp(Teeth[2].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                        Teeth[3].material.color = Color.Lerp(Teeth[3].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);*/

            //Body

            changeColorS(Body.materials[0], BodyChangeCol, BodyChangeCol_0, duration);
            changeColorS(Body.materials[1], BodyChangeCol, BodyChangeCol_0, duration);

            /*            Body.materials[0].color = Color.Lerp(Body.materials[0].color, BodyInitCol, lerpSpeed * Time.deltaTime); 
                        Body.materials[1].color = Color.Lerp(Body.materials[1].color, BodyInitCol, lerpSpeed * Time.deltaTime);*/

            //Eyes
            changeColorS(Body.materials[2], Eye_3, Eye_3_0, duration);
            /*Body.materials[2].color = Color.Lerp(Body.materials[2].color, EyeInitColor, lerpSpeed * Time.deltaTime);*/
        }

        /*
                if (SC == 1)
                {
                    //Teeth
                    Teeth[0].material.color = Color.Lerp(Teeth[0].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime); 
                    Teeth[1].material.color = Color.Lerp(Teeth[1].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                    Teeth[2].material.color = Color.Lerp(Teeth[2].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                    Teeth[3].material.color = Color.Lerp(Teeth[3].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);

                    //Body
                    Body.materials[0].color = Color.Lerp(Body.materials[0].color, BodyInitCol, lerpSpeed * Time.deltaTime);
                    Body.materials[1].color = Color.Lerp(Body.materials[1].color, BodyInitCol, lerpSpeed * Time.deltaTime);

                    //Eyes
                    Body.materials[2].color = Color.Lerp(Body.materials[2].color, Eye_1, lerpSpeed * Time.deltaTime);
                }

                if (SC == 2)
                {
                    //Teeth
                    Teeth[0].material.color = Color.Lerp(Teeth[0].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);
                    Teeth[1].material.color = Color.Lerp(Teeth[1].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);
                    Teeth[2].material.color = Color.Lerp(Teeth[2].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);
                    Teeth[3].material.color = Color.Lerp(Teeth[3].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);

                    //Body
                    Body.materials[0].color = Color.Lerp(Body.materials[0].color, BodyInitCol, lerpSpeed * Time.deltaTime);
                    Body.materials[1].color = Color.Lerp(Body.materials[1].color, BodyInitCol, lerpSpeed * Time.deltaTime);

                    //Eyes
                    Body.materials[2].color = Color.Lerp(Body.materials[2].color, Eye_1, lerpSpeed * Time.deltaTime);
                }

                if (SC == 3)
                {
                    //Teeth
                    Teeth[0].material.color = Color.Lerp(Teeth[0].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);
                    Teeth[1].material.color = Color.Lerp(Teeth[1].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);
                    Teeth[2].material.color = Color.Lerp(Teeth[2].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);
                    Teeth[3].material.color = Color.Lerp(Teeth[3].material.color, TeethInitialColor, lerpSpeed * Time.deltaTime);

                    //Body
                    Body.materials[0].color = Color.Lerp(Body.materials[0].color, BodyInitCol, lerpSpeed * Time.deltaTime);
                    Body.materials[1].color = Color.Lerp(Body.materials[1].color, BodyInitCol, lerpSpeed * Time.deltaTime);

                    //Eyes
                    Body.materials[2].color = Color.Lerp(Body.materials[2].color, Eye_2, lerpSpeed * Time.deltaTime);
                }

                if (SC == 4)
                {
                    //Teeth
                    Teeth[0].material.color = Color.Lerp(Teeth[0].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);
                    Teeth[1].material.color = Color.Lerp(Teeth[1].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);
                    Teeth[2].material.color = Color.Lerp(Teeth[2].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);
                    Teeth[3].material.color = Color.Lerp(Teeth[3].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);

                    //Body
                    Body.materials[0].color = Color.Lerp(Body.materials[0].color, BodyInitCol, lerpSpeed * Time.deltaTime);
                    Body.materials[1].color = Color.Lerp(Body.materials[1].color, BodyInitCol, lerpSpeed * Time.deltaTime);

                    //Eyes
                    Body.materials[2].color = Color.Lerp(Body.materials[2].color, Eye_2, lerpSpeed * Time.deltaTime);
                }

                if (SC == 5)
                {
                    //Teeth
                    Teeth[0].material.color = Color.Lerp(Teeth[0].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);
                    Teeth[1].material.color = Color.Lerp(Teeth[1].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);
                    Teeth[2].material.color = Color.Lerp(Teeth[2].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);
                    Teeth[3].material.color = Color.Lerp(Teeth[3].material.color, TeethChangeColor, lerpSpeed * Time.deltaTime);

                    //Body
                    Body.materials[0].color = Color.Lerp(Body.materials[0].color, BodyChangeCol, lerpSpeed * Time.deltaTime); 
                    Body.materials[1].color = Color.Lerp(Body.materials[1].color, BodyChangeCol, lerpSpeed * Time.deltaTime);

                    //Eyes
                    Body.materials[2].color = Color.Lerp(Body.materials[2].color, Eye_3, lerpSpeed * Time.deltaTime);
                }*/
    }

    void colorChanger(Material mr, Color Color_1, Color Color_2, float speed)
    {        
        mr.color = Color.Lerp(mr.color, Color_2, speed * Time.deltaTime);
    }
}
