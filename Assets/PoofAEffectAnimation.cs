using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoofAEffectAnimation : MonoBehaviour
{
    public ParticleSystem vfxHit;
    public bool isImp = false;
    public void hitVFX()
    {
        vfxHit.Play();
        
    }

    public void AudioHit()
    {
        if (isImp) EvenManager.TriggerSFXOneShotPlayEvent(AudioID.impAttack); else { EvenManager.TriggerSFXOneShotPlayEvent(AudioID.demonAttack); }
    }
}
